import React from "react";
import ReactFlow from "react-flow-renderer";
import GraphKB from "./GraphKB";

const elements = [
    {
        id: "1",
        type: "input", // input node
        data: { label: "1 Input Node" },
        position: { x: 100, y: 100 },
        sourcePosition: "right",
        draggable: false,
    },
    // default node
    {
        id: "2",
        // you can also pass a React component as a label
        data: { label: <div>Default Node</div> },
        position: { x: 400, y: 150 },
        sourcePosition: "right",
        targetPosition: "left",
        draggable: false,
    },
    {
        id: "3",
        type: "input", // input node
        data: { label: "2 Input Node" },
        position: { x: 100, y: 200 },
        sourcePosition: "right",
        draggable: false,
    },
    // animated edge
    { id: "e1-2", source: "1", target: "2", type: "straight" },
    { id: "e2-3", source: "3", target: "2", type: "straight" },
];

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            elements: elements,
            fact: String,
        };
    }

    getFact = (event) => {
        this.setState((state) => {
            state.fact = event.target.value;
            return state;
        });
    };

    addNode = () => {
        this.setState((state) => {
            state.elements = [
                {
                    id: new Date().getTime().toString(),
                    data: { label: this.state.fact },
                    position: { x: 300, y: 25 },
                    sourcePosition: "right",
                    targetPosition: "left",
                },
            ].concat(state.elements);
            return state;
        });
    };

    render() {
        return (
            <div style={{ height: 1000}}>
                {/* <ReactFlow elements={this.state.elements} />
        <button onClick={this.addNode}>Add node</button>
        <input onChange={this.getFact}></input> */}
                <GraphKB id="4"></GraphKB>
            </div>
        );
    }
}

export default App;
