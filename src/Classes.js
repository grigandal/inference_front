import { Tooltip } from "antd";

class Fact {
    id = 0;
    cf = 0;
    name = "";

    constructor({ id, name, cf }) {
        this.name = name;
        this.id = id;
        this.cf = cf;
    }
}

class Rule {
    id = 0;
    number = 0;
    cf = 0;
    premise = [];
    conclusion = [];

    constructor({ id, number, cf, premise, conclusion }) {
        this.id = id;
        this.number = number;
        this.cf = cf;
        this.premise = premise;
        this.conclusion = conclusion;
    }
}

class KBNode {
    store = undefined;
    parentNodes = [];
    childNodes = [];
    constructor(store, parentNodes = [], childNodes = []) {
        this.store = store;
        this.parentNodes = parentNodes;
        this.childNodes = childNodes;
    }

    get data() {
        return {
            label: this.isFact ? (
                <>
                    <Tooltip title={this.store.name}>
                        {this.store.name.length > 15
                            ? this.store.name.slice(0, 12) + "..."
                            : this.store.name}
                    </Tooltip>{" "}
                    {this.store.cf}
                </>
            ) : (
                <div style={{ textAlign: "center" }}>
                    R{this.store.number}
                    <br />
                    {this.store.cf}
                </div>
            ),
            node: this,
        };
    }

    get isFact() {
        return this.store instanceof Fact;
    }

    get minLevel() {
        return this.parentNodes.length
            ? this.parentNodes
                  .map((node) => node.minLevel)
                  .sort((a, b) => a - b)[0] + 1
            : 0;
    }

    get maxLevel() {
        return this.parentNodes.length
            ? this.parentNodes
                  .map((node) => node.maxLevel)
                  .sort((a, b) => b - a)[0] + 1
            : 0;
    }
}

class KB {
    initial = {};
    roots = [];
    constructor({ facts, rules }) {
        this.initial = { facts, rules };
    }

    static _selectRuleNode = (node, selector = (node) => true) => {
        if (!node.isFact && selector(node)) {
            return node;
        } else {
            for (let i in node.childNodes) {
                let childNode = node.childNodes[i];
                let rule = KB._selectRuleNode(childNode, selector);
                if (rule) {
                    return rule;
                }
            }
        }
    };

    static _selectFactNode = (node, selector = (node) => true) => {
        if (node.isFact && selector(node)) {
            return node;
        } else {
            for (let i in node.childNodes) {
                let childNode = node.childNodes[i];
                let rule = KB._selectFactNode(childNode, selector);
                if (rule) {
                    return rule;
                }
            }
        }
    };

    selectRuleNode = (selector = (node) => true) =>
        this.roots.reduce(
            (accumulator, node) =>
                accumulator ? accumulator : KB._selectRuleNode(node, selector),
            undefined
        );

    selectFactNode = (selector = (node) => true) =>
        this.roots.reduce(
            (accumulator, node) =>
                accumulator ? accumulator : KB._selectFactNode(node, selector),
            undefined
        );

    get rootFacts() {
        return this.initial.facts.filter(
            (fact) =>
                !this.initial.rules.filter((rule) =>
                    rule.conclusion.includes(fact.id)
                ).length
        );
    }

    get rules() {
        return this.initial.rules;
    }

    get facts() {
        return this.initial.facts;
    }

    getFactById = (id) => this.facts.filter((fact) => fact.id === id)[0];

    getRuleById = (id) => this.rules.filter((rule) => rule.id === id)[0];

    getFactChildrenByFactId = (id) =>
        this.rules.filter((rule) => rule.premise.includes(id));

    getRuleChildrenByRuleId = (id) =>
        this.getRuleById(id).conclusion.map((c) => this.getFactById(c));

    _build = (nodes) => {
        nodes.forEach((node) => {
            if (node.isFact) {
                node.childNodes = this.getFactChildrenByFactId(
                    node.store.id
                ).map((rule) => {
                    let rNode = this.selectRuleNode(
                        (r) => r.store.id === rule.id
                    );
                    if (rNode) {
                        return rNode;
                    }
                    return new KBNode(new Rule(rule));
                });
            } else {
                node.childNodes = this.getRuleChildrenByRuleId(
                    node.store.id
                ).map((fact) => {
                    let fNode = this.selectFactNode(
                        (f) => f.store.id === fact.id
                    );
                    if (fNode) {
                        return fNode;
                    }
                    return new KBNode(new Fact(fact));
                });
            }
            node.childNodes.forEach((childNode) =>
                !childNode.parentNodes
                    .map((p) => p.store.id)
                    .includes(node.store.id)
                    ? childNode.parentNodes.push(node)
                    : null
            );
            this._build(node.childNodes);
        });
    };

    build = () => {
        this.roots = this.rootFacts.map((fact) => new KBNode(new Fact(fact)));
        this._build(this.roots);
    };

    _getNodesByLevel = (level, nodes, max = true) => {
        let result = nodes.filter((node) =>
            max ? node.maxLevel === level : node.minLevel === level
        );
        nodes.forEach((node) => {
            result = result.concat(
                this._getNodesByLevel(level, node.childNodes, max).filter(
                    (node) => !result.includes(node)
                )
            );
        });
        return result;
    };

    getNodesByLevel = (level, max = true) =>
        this._getNodesByLevel(level, this.roots, max);

    edges = (elements) =>
        elements.reduce(
            (accumulator, element, i, l) =>
                accumulator.concat(
                    element.node.childNodes.map((childNode) =>
                        Object({
                            id: `e${element.id}-${
                                l.filter((e) => e.node === childNode)[0].id
                            }`,
                            source: element.id,
                            target: l.filter(
                                (e) =>
                                    e.node.isFact === childNode.isFact &&
                                    e.node.store.id === childNode.store.id
                            )[0].id,
                            arrowHeadType: "arrowclosed",
                            type: "straight",
                        })
                    )
                ),
            []
        );

    get graph() {
        let elements = [];
        let level = 0;
        let levels = [];
        let nodes = this.getNodesByLevel(level);

        while (nodes.length) {
            let lvl = nodes.sort((a, b) => a.store.number - b.store.number);
            levels.push(lvl);
            level += 1;
            nodes = this.getNodesByLevel(level);
        }
        let maxWidth =
            levels.map((l) => l.length).sort((a, b) => b - a)[0] * 150;
        for (let level in levels) {
            let nodes = levels[level];
            let width = nodes.length * 150;
            elements = elements.concat(
                nodes.map((node, i) =>
                    Object({
                        position: {
                            x: level * 175 + (node.isFact ? 0 : 50),
                            y:
                                i * 75 +
                                parseInt((maxWidth - width) / 4) +
                                (node.isFact ? 0 : -5),
                        },
                        sourcePosition: "right",
                        targetPosition: "left",
                        data: node.data,
                        a: node.data.label,
                        draggable: false,
                        node,
                        type: node.isFact ? "default" : "rule",
                    })
                )
            );
        }
        elements = elements.map((element, i) =>
            Object({ id: i.toString(), ...element })
        );
        elements = elements.concat(this.edges(elements));

        return elements;
    }
}

export default KB;
