import { Tooltip, Popover } from "antd";
import React from "react";
import ReactFlow from "react-flow-renderer";
import { getMarkerEnd, getBezierPath, Handle } from "react-flow-renderer";
import KB from "./Classes";

class HoveredEdge extends React.Component {
    getStraightPath = ({ sourceX, sourceY, targetX, targetY }) => {
        return `M ${sourceX} ${sourceY} L ${targetX} ${targetY}`;
    };

    render() {
        // debugger;
        // const edgePath = getBezierPath(this.props);
        const edgePath = this.getStraightPath(this.props);
        const markerEnd = getMarkerEnd(this.props.arrowHeadType);
        return (
            <path
                id={this.props.id}
                style={this.props.style}
                className="hovered-stroke"
                d={edgePath}
                markerEnd={markerEnd}
                fill="transparent"
            ></path>
        );
    }
}

class HoveredNode extends React.Component {
    render() {
        // console.log(this.props);
        return (
            <div>
                <Handle type="source" position={this.props.sourcePosition} />
                <div className="hovered-node">
                    <Popover title={this.props.data.node.store.name}>
                        {this.props.data.label}
                    </Popover>
                </div>
                <Handle type="target" position={this.props.targetPosition} />
            </div>
        );
    }
}

class RuleNode extends React.Component {
    componentDidMount(){
        
    }
    render() {
        // console.log(this.props);
        return (
            <>
                <Handle type="source" position={this.props.sourcePosition} />
                <div
                    style={{
                        padding: 5,
                        border: "1px solid black",
                        borderRadius: 20,
                    }}
                >
                    {this.props.data.label}
                </div>
                <Handle type="target" position={this.props.targetPosition} />
            </>
        );
    }
}

export default class GraphKB extends React.Component {
    constructor({ id, ...props }) {
        super(props);
        this.state = {
            id,
            hovered: undefined,
            kb: undefined,
        };
    }

    async componentDidMount() {
        let response = await fetch(
            `http://127.0.0.1:8000/api/knowledge_bases/${this.state.id}/`
        );
        let response2 = await fetch(
            `http://127.0.0.1:8000/api/knowledge_bases/${this.state.id}/list_facts/`
        );
        switch (response.status + response2.status) {
            case 400:
                let json = await response.json();
                let json2 = await response2.json();
                this.setState((state) => {
                    state.kb = new KB({ facts: json2, rules: json.rules });
                    state.kb.build();
                    return state;
                });
                break;
            default:
                let t = await response.text();
                console.log(t);
        }
    }

    get graph() {
        let graph = this.state.kb ? this.state.kb.graph : [];
        graph = graph.map((e) => {
            if (this.state.hovered) {
                if (
                    e.source === this.state.hovered.id ||
                    e.target === this.state.hovered.id
                ) {
                    e.type = "nodeHovered";
                } else if (e.source || e.target) {
                    e.type = "straight";
                } else if (
                    e.id === this.state.hovered.id &&
                    e.type !== "rule"
                ) {
                    e.type = "nodeHovered";
                }
            } else if (e.source || e.target) {
                e.type = "straight";
            } else if (e.type !== "rule") {
                e.type = "default";
            }

            return e;
        });
        return graph;
    }

    onNodeHover = (event, node) =>
        this.setState((state) => Object({ ...this.state, hovered: node }));

    onNodeOut = (event, node) =>
        this.setState((state) => Object({ ...this.state, hovered: undefined }));

    render() {
        // return <div>{JSON.stringify(this.state.rules)}</div>;
        console.log(this.graph[0] ? this.graph[0].type : "");
        return (
            <div style={{ height: "100%" }}>
                <ReactFlow
                    style={{ height: "100%" }}
                    onNodeMouseEnter={this.onNodeHover}
                    onNodeMouseLeave={this.onNodeOut}
                    elements={this.graph}
                    edgeTypes={{
                        nodeHovered: HoveredEdge,
                    }}
                    nodeTypes={{
                        nodeHovered: HoveredNode,
                        rule: RuleNode,
                        // factInWM:
                    }}
                />
            </div>
        );
    }
}
